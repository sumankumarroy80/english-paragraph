Completing a Story ‡jLvi mgq wb‡Pi wbqg¸‡jv g‡b ivL‡e
1. cÖkœc‡Î mvavš^Z GKwU cwiwPZ ev AcwiwPZ M‡íi Ask we‡kl ev outlines ‡`Iqv _v‡K| cÖ`Ë Ask we‡kl Abymv‡I mnR I mvejxj fvlvq MíwU wjL‡Z nq|
2. M‡í ïiæ‡Z GKUv Title ev wk‡ivbvg Aek¨B †hvM Ki‡Z n‡e|
3. MíwU g~jZ past tense e¨envi K‡I wjL‡Z nq|
4. M‡íi moral lesson ev ˆbwZK wkÿvi Av‡jv‡K Title wjL‡j me‡P‡q fv‡jv nq| Dc‡iv³ g‡Wj story wUi moral lesson n‡jv  A friend in need is a friend indeed A_©vr wec‡`i eÜzB cÖK…Z eÜz; hv M‡íi Title wn‡m‡e e¨envi Kiv n‡q‡Q| B‡”Q Ki‡j M‡íi Title wU †Zvgiv Ab¨fv‡eI wjL‡Z cvi| †hgb : Two Friends A_ev Two Friends And The Bear| Z‡e Zv cÖ`Ë Title wUi g‡Zv GZ fv‡jv n‡e bv|
5. hw` Mí Rvbv bv _v‡K ZeyI †Kv‡bv mgm¨v †bB| cÖkœc‡Îi Outline e¨envi K‡i wb‡Ri g‡bi g‡Zv K‡i GKwU Mí †jLvi †Póv Ki‡e| Z‡e MíwU Aek¨B mgvß Ki‡Z n‡e|

Completing a Story ‡jLvi ¸iæZ¡c~Y© wUcm&
  M‡íi cÖ`Ë Outline Gi Av‡jv‡K g‡b g‡b m¤ú~Y© M‡íi GKwU plot ˆZwi Ki‡e|
  Mí †hfv‡eB †jLv †nvK bv †Kb Gi g‡a¨ †hb GKwU moral lesson (‰bwZK wkÿv) _v‡K|
  M‡íi †hLv‡b m¤¢e direct speech e¨envi Ki‡e hv‡Z MíwU RxešÍ n‡q I‡V| G‡ÿ‡Î Bbfvi‡UW Kgv (Ò......Ó) e¨envi Ki‡Z fzj K‡iv bv|
  cÖ`Ë Outline †_‡K Mí †jLvi mgq †Lqvj ivL‡e †hb NUbvcÖevn AcwiewZ©Z _v‡K|
  M‡íi Outline KL‡bvB cwieZ©b Ki‡e bv, A_©vr cÖ`Ë Outline wU cy‡ivcywi e¨envi Ki‡e|
  Ah_v evowZ K_v eR©b Ki‡e|




GLv‡b K‡qKwU ¸iæZ¡c~Y© Completing a Story Gi bgybv †`Lv‡bv n‡jv|

Read the beginning of the following story and complete it in your own way. Give a title to it.
There lived an old farmer who had three sons. They used to quarrel with one another. One day the farmer told his sons to bring some sticks and tie them in a bundle. He told them to break the bundlei They tried but could not break the bundle.........................................................................
Ans.                                        Unity is Strength / An Old Farmer and His Sons
There lived an old farmer who had three sons. They used to quarrel with one 'another. One day the farmer told his sons to bring some sticks and tie them in a bundle. He told them to break the bundle. They tried but could not break the bundle.
Each of them told his father that it was quite impossible to break it because the bundle was'ver strong. The farmer was an intelligent and experienced man. He said to them, "All right. Now I'll do one thing for you and you'll, please do it." The sons readily agreed to do that.
Then the farmer untied the bundle. He then gave one stick to each of them and asked them to break it. This time each son could easily break the stick.
The sons got curious to know the reason of those activities. They said to their father, "Why have you made us do these jobs, dear father? What good will come of it?"
Then the wise father told the sons, "Wait a bit, dear sons. I'm making it clear to you." Now the father drew their attention to the tied bundle and the separate sticks. He explained to them that when the sticks were tied in a bundle, it was impossible for them to break the sticks. But when the sticks were made separate from one another, it was very'easy to break them.
Through the fact, the farmer wanted to make his sons learn that they were like the sticks. When they would be strongly united (tied), nobody could harm them. So They should always remain united as the proverb goes 'unity is strength'. Thus the old farmer could effectively teach his sons the valuable proverb, "United we stand, divided we fall."




3. Read the beginning of the following story and complete it in your own way. Give a title to it.
On a hot summer day a crow felt very thirsty. It was flying to and fro looking for water. But it did not find water anywhere. At last it found a pitcher of water at some distance. The pitcher had some water but it was beyond the reach of the crow…….
Ans.                   Where There is a Will There is a Way/
                           A Thirsty Crow / An Intelligent Crow
On a hot summer day a crow felt very thirsty. It was flying to and fro looking for water. But it did not find water anywhere. At last it found a pitcher of water at some distance. The pitcher had some water but it was beyond the reach of the crow. It tried its best to drink water but its bill could not reach into it. It became very sad and hopeless.
Suddenly the crow saw some small pieces of stone near the jar. A good idea came into its head. It then picked up the stones and dropped them into the jar one by one. As a result, the water came up to the brim. Then the crow drank water from it to its'- heart's content and flew away with a joyful mind.
In fact where there is a will there is a way. So we should not be hopeless. Rather we should try every possible way to meet our utmost need.

4. Read the beginning of the following story and complete it in your own way. Give a title to it.
There lived a clever fox in a jungle. One day he fell into a trap as he was passing through the jungle. He could get out of the trap but lost his tail behind. Without his tail he looked strange and he felt sad           
Ans.                                                A Fox Without a Tail
There lived a clever fox in a jungle. One day he fell into a trap as he was passing through the jungle. He could get out of the trap but lost his tail behind. Without his tail he looked strange and he felt sad. But the fox was very cunning and he hit upon a plan. He invited all the foxes to come to a meeting. All of them came.
Then the fox said, "My dear friends, listen to me please. I've invented a new thing, the tails of ours are useless. They look ugly and are always dirty. So, we should cut off our tails."
All the foxes listened to the cunning fox. One of the wise old foxes was also listening. He said, "My friend, your plan is very interesting but foolish. As you've lost your tail, you want us to cut off our
tails.
At this the fox without a tail felt ashamed and left the place quickly.




5.    Read the beginning of the following story and complete it in your own way. Give a title to it.
There was a shepherd boy. He used to graze a flock of sheep near a forest. It was not far from his village. One day he wanted to make a fun with the villagers. So, he began to shout, "Wolf! Wolf! Help! Help!"  
Ans.                                                    A Liar Shepherd
There was a shepherd boy. He used to graze a flock of sheep near a forest. It was not far from his village. One day he wanted to make a fun with the villagers. So, he began to shout, "Wolf! Wolf! Help! Help!"
The villagers heard his cry and ran to help him. But when they came near him, they found no wolf there and the boy laughed at them. The villagers were fooled by the cunning boy. So, they went away.
After a few days he made the same fun. The villagers came again to help him but found no wolf there.
But one day a wolf really fell upon the sheep and began to kill his sheep. The boy became very afraid and cried, "Wolf! Wolf! Help! Help! A wolf is in my sheep! Come and help me!" The villagers heard him but they thought that it was his another fun. No one came to help him. The wolf feasted on the sheep and finally killed the shepherd. So, it is very unwise to make fun by telling lies.

6.    Read the beginning of the following story and complete it in your own way. Give a title to it.
A large number of mice lived in a farm. Once an old cat arrived at the place. It spread terror among the mice. None dared come outside in fear of being killed by the cat. All the mice decided to hold a conference to seek a solution.............................................................
Ans.                             Who's to Bell the Cat / Easier Said than Done
A large number of mice lived in a farm. Once an old cat arrived at the place. It spread terror among the mice. None dared come outside in fear of being killed by the cat. All the mice decided to hold a conference to seek a solution.
Taking advantage of the cat's absence, one day the mice of all ages got together in the conference. Each one put forward a suggestion, but none of the ideas was really practical.
"Let's make a big trap," one mouse suggested. This idea was turned down and, another said, "What about poisoning her? But nobody knew which poison could kill cats. One young widow, whose husband has fallen a prey to the ferocious cat, angrily proposed, "Let's cut her claws and teeth, so that she can do no more harm." But the conference did not approve of the widow's idea. At last, a mouse, wiser than others, waving a bell, called for silence. "We'll tie this bell to the cat's neck, them we'll always know where she is! We'll have time to escape."
All of the mice clapped at the wise mouse's words and everyone congratulated him on his good idea.
However, the wise mouse rang the bell again for silence. "We must decide who's going to bell the cat," he said. There was no sound except a faint murmur : "I can"t because ......................... 11
Nobody was brave enough to come forward to materialise the idea and the conference ended in smoke.




7. Read the beginning of the following story and complete it in your own way. Give a title to it.
A hare is a speedy animal. It can run very fast. But a tortoise moves slowly. It has heavy shell on its back. Its legs are short. So it cannot ion as fast as other animals. Once upon a time a hare was very proud of his speed       
Ans. Slow and Steady Wins the Race / A Race Between a Hare and a Tortoise
A hare is a speedy animal. It can run very fast. But a tortoise moves slowly. It has heavy shell on its back. Its legs are short. So it cannot run as fast as other animals. Once upon a time a hare was very proud of his speed. He laughed at the tortoise and said, "You have as many legs as I have. But I am sure, you cannot run as fast as I can. Perhaps, you cannot run at all." The tortoise became very angry. He answered quietly, "You can run quite fast, no doubt. But I think you can talk even faster than you can run."
Then the tortoise made a proposal to run a race with the hare. The hare agreed to the proposal gladly.
On a nice morning the race began. Other beasts of the forest were invited to watch the race. Afte running for a while, the hare reached near the goal. Now he thought that the tortoise would not be able to run past him and the tortoise was lagging far behind. Thinking this, the hare wanted to have a little. rest and fell asleep.
But the tortoise continued running. He came near the sleeping hare and ran past him. As a result, he came near the destination while the hare was sleeping. In the meantime, the hare woke up and spared no pains to catch up the tortoise but he failed.
Winning the race the tortoise laughed a hearty laugh and the hare became ashamed of his proud speech. It proves, "Slow and steady wins the race."

8.    Read the beginning of the following story and complete it in your own way. Give a title to it.
One day two cats stole a piece of bread. They failed to divide it in two equal parts as both of them wanted the larger part. They fought for that. At last, they agreed to put up their problem to a monkey. All the animals of the forest knew him as the wise animal...
Ans.                                  Bread Dividing by a Cunning Monkey
One day two cats stole a piece of bread. They failed to divide it in two equal parts as both of them wanted the larger part. They fought for that. At last, they agreed to put up their problem to a monkey. All the animals of the forest knew him as the wise animal.
They went to the monkey and asked him to divide the bread into two equal parts. The monkey behaved very gently with them. He said that it would be costly for them. It was better for them to settle it among themselves. But the cats said that as they had failed to do so, they had come to him in order to make a way out of the problem.
Then the monkey brought a pair of scales and broke the bread into two pieces. He put the pieces on the scales. The monkey told that one piece was heavier than the other. He took the heavier piece and bit a small piece from it. Then the other piece became heavier. The monkey took a portion from that piece and put it on the scales. The pieces remained unequal even now. And the monkey did the same thing again and again. After some time there remained only one small piece of the bread.
Then the cats asked the monkey not to divide the bread. They said that they would divide it equally themselves. They asked for that small piece of bread.
Then the monkey said angrily, "No, this piece is mine. I've suffered much to divide it, into equal, pieces." Saying this, the monkey devoured the rest of the bread.


Read the beginning of the following story and complete it in your own way. Give a title to it.
Once a lion was sleeping in a forest.. Suddenly a mouse came there. It did not notice the sleeping lion. It was running about and playing happily. By chance it ran over the face of the lion. It awoke the lion.
Ans.                                    A Kind Lion and a Grateful Mouse
Once a lion was sleeping in a forest. Suddenly a mouse came there. It did not notice the sleeping lion. It was running about and playing happily. By chance it ran over the face of the lion. It awoke the lion. At this the lion grew very angry, held him tightly and would not let him go.
"Please let me go, Sir", said the mouse, "One day I'll help you." The lion laughed at him. "How could a little mouse help a big lion?" he thought. "Very well". he said, "I'll let you go. But you must run more carefully."
The mouse was very grateful. "Thank you", he said, "You're very kind."
The next week the mouse was again looking for something to eat. He saw then that the lion was tied into a net under a tree. The mouse said, "Sir, I'll help you". He cut the ropes of the net with his sharp teeth one after another and soon the lion was free. He was very grateful to the mouse.
He thanked the mouse. Then he realized that help may come even from the smallest of our friends. So every entity, smaller or larger has importance of its own in this earth.

10. Read the beginning of the following story and complete it in your own way. Give a title to it.
There lived a farmer in a village. He had a wonderful goose. The goose laid an egg of gold everyday, The farmer was very greedy. He thought that there were many eggs in the bellyof the goose. He wanted to have all the eggs at a time. Thus he wished to be rich .....................................................
Ans.                             Grasp All, Lose All / A Greedy Farmer
There lived a farmer in a village. He had a wonderful goose. The goose laid an egg of gold everyday. The farmer was very greedy. He thought that there were many eggs in the belly of the goose. He wanted to have all the eggs at a time. Thus he wished to be rich.
One day he hit upon a plan that he would kill the goose and get all the eggs of gold from its belly. Then he would sell the eggs and become rich at once.
The farmer told his wife about his plan. His wife was wise but not greedy. She said to her husband,
"Don't be greedy. Be happy with what we've."
But the farmer did not listen to his wife. He killed the goose with a sharp knife. Then he cut its belly to get all the eggs at a time. But alas! He found no eggs in it. Thus the greedy farmer lost his useful goose. Then he understood the lesson, 'grasp all, lose all.'




11.  Read the beginning of the following story and complete it in your own way. Give a title to it.
One night the boy Bayazid was sitting beside his ailing mother. She was lying on her bed. At midnight she woke up and wanted a glass of water. ...................
Ans.                                  Bayazid's Love for His Mother
One night the boy Bayazid was sitting beside his ailing mother. She was lying on her bed. At midnight she woke up and wanted a glass of water. Bayazid went to the kitchen to bring water. But the pitcher was empty. He thought to fetch water from a distant fountain. He went alone to the fountain with the pitcher and came back home with the pitcher full of water. In the meantime, his mother was fast asleep again. He did not want to disturb his ailing mother. So, he remained standing beside his mother's bed. In the morning, Bayazid's mother awoke. She became astonished to see her son standing with the glass of water beside her bed. She embraced her son with deep love and prayed to Almighty Allah for her son, In his later life, Bayazid became a great saint.

12. Read the beginning of the following story and complete it in your own way. Give a title to it.
There lived a woodcutter in a village. One day he was cutting wood near a pond. Suddenly, his axe fell, into the pond. The pond was very deep. The woodcutter did not know how to swim or dive. So, he was sitting there sadly. ........................................
Ans.                                         Honesty is the best policyj
                           An Honest Woodcutter and the Beautiful Fairy
There lived a woodcutter in a village. One day he was cutting wood near a pond. Suddenly, his axe fell into the pond. The pond was very deep. The woodcutter did not know how to swim or dive. So, he was sitting there sadly.
Then a wonderful thing happened. A beautiful fairy appeared before the woodcutter. She asked him in a sweet voice, "Why are you so sad? Why are you not cutting wood?"
The woodcutter replied sadly, "My axe has fallen into the pond. I can't cut wood now."
The fairy then showed him an axe made of silver. She asked him if it was his axe. The woodcutter saw the axe and said, "It's not my axe."
The fairy showed him another axe made of gold. She asked, Is this the axe that you lost?" The woodcutter said, "No, it's not. my axe is made of iron. It was old." The fairy showed him the lost axe with a wooden handle. The woodcutter then said happily that it was his axe.
The fairy became very pleased and gave him the gold and the silver axes too. Then the woodcutter became rich and began to live happily. Thus the woodcutter got the reward of his honesty.




13.    Read the beginning of the following story and complete it in your own way. Give a title to it.
Once a hungry fox was searching for food in a forest. After a while he came to a vineyard. He was very tempted at the sight of the ripe grapes hanging very high. ...................................
Once a hungry fox was searching for food in a forest. After a while he came to a vineyard. He was very tempted at the sight of the ripe grapes hanging very high. He thought to himself and wished to have a delicious meal of the ripe grapes.
He could not resist his temptation to have the taste of the ripe grapes. So he jumped and jumped to reach the grapes. But the bunches of the ripe grapes were too high for the fox to reach.
The poor fox became very tired as he repeatedly and ceaselessly jumped to reach the bunches of the ripe grapes. So he went away dejectedly and broken heartedly.
He consoled himself by murmuring that the grapes were sour and so he did not have them.

14.    Read the beginning of the following story and complete it in your own way. Give a title to it.
One day a crow stole a piece of meat from a butcher's shop. It flew away with it in its beak and sat on the branch of a tree. .................................
Ans.                                   The Clever Fox and the Foolish Crow
One day a crow stole a piece of meat from a butcher's shop. It flew away with it in ils beak and sat on the branch of a tree. A jackal was passing under the tree. He saw the crow with the meat. His mouth watered seeing the piece of meat. He hit upon a plan to have it.
He said to the crow, "Oh beautiful Crow! Your complexion is so fair. I think you can sing nicely. Would you please sing a song for me?"
At first the crow did not want to sing. But as the fox carried on praising her she felt proud of her tongue.
Finally, she started to sing a song. But no sooner had she opened her mouth, the piece of meat fell off from her mouth. The sly fox took it and ran away.

15.    Read the beginning of the following story and complete it in your own way. Give a title to it.
Once an ant was very thirsty. He went to a pond to drink water. When he began to drink water, a wave swept him away. He was about to drown.........................................
Ans.                                               An Ant and a Dove /
                                         A Friend in Need is a Friend Indeed
Once an ant was very thirsty. He went to a pond to drink water. When he began to drink water, a wave swept him away. He was about to drown.
There was a tree on the bank of the pond. A dove was sitting on a branch of the tree. He noticed that the ant was about to drown. The dove felt pity for the ant. He wanted to save the ant. So he dropped a leaf in front of the ant. The ant got on the leaf and was saved.
Another day the ant was seeking food under that tree. Then he noticed that a hunter had aimed at the dove. The dove did not know it. The ant thought, "I must save the life of the dove as it saved my life". So he bit on the right leg of the hunter. At this the hunter's hands trembled and the bullet missed its aim. Then the dove flew away and was saved.




16.    Read the beginning of the following story and complete it in your own way. Give a title to it.
Once there lived a wolf in a wood. Once he killed a lamb. As he went on eating the flesh of the lamb, a bone stuck in his throat. It caused him great pain. He requested everyone he met to take out the bone. But no one helped him. They were afraid of the wolf. .........................................................
Ans.                                       An Ungrateful Wolf and a Crane
Once there lived a wolf in a wood. Once he killed a lamb. As he went on eating the flesh of the lamb, a bone stuck in his throat. It caused him great pain. He requested everyone he met to take out the bone. But no one helped him. They were afraid of the wolf.
At last, he saw a crane. He ran to him and said, "Oh, little friend, please help me. I'm dying with pain. Take out the bone from my throat with your long beak. I'll give you a good reward."
The crane felt pity for the wolf. He put his long beak into the throat of the wolf. The bone was drawn out quite easily. The wolf now felt at ease.
"Now, sir, please give me my reward," said the crane. "A reward! What a fool you're! You brought out your head quite safely from my mouth. I did not bit'it. Wasn't that good enough? Make haste and leave," replied the wolf.
The poor crane was frightened. He left the place in a hurry.

17.     Read the beginning of the following story and complete it in your own way. Give a title to it.
Once upon a time there was a king called Midas. He was extremely fond of gold. Although he had a lot of it, he wanted more. He thought if he had the golden touch, he would be the happiest man……..
Ans. King Midas : The Golden Touch / The More Man Gets, The More He Wants
Once upon a time there was a king. called Midas. He was extremely fond of gold. Although he had a lot of it, he wanted more. He thought if he had the golden touch, he would be the happiest man. A wise God granted his wish promptly.
One morning he got up early in the morning and started walking about in his garden as usual. He touched a white rose in the garden, and he saw, in his utter surprise, that the flower had turned into a piece of white gold. He was amazed. He then touched another plant which, in an instant, turned into gold. It was really amusing to him. He started playing with his new found blessing.
Right at this moment, his only daughter entered hardens. He embraced his daughter to share his delight with her. To his sheer grief, he found her daughter being turned into a statue of gold. He became very shocked at this turn. He could not bear the pair caused by the loss of his daughter. He prayed to the God to take back the destructive blessing. After few moments, the girl came to life. The king became happy.




18.     Read the beginning of the following story and complete it in your own way. Give a title to it.
Once upon a time there was a king who was very fond of knowing his future from the astrologers. A famous astrologer happened to stop on his way to Delhi. The king called on him to know about his future. The astrologer told him something unpleasant. ...........................
Ans.                                        A King and an Astrologer / The Clever Astrologer
Once upon a time there was a king who was very fond of knowing his future from the astrologers. A famous astrologer happened to stop on his way to Delhi. The king called on him to know about his future. The astrologer told him something unpleasant. The king became very angry with him. The king ordered his men to hang the astrologer the next day at a public place.
The astrologer became very afraid. He could not sleep all the night worrying over his death. At last he found a way out.
The next day when the people had crowded round the astrologer, he began to smile instead of weeping. The king got astonished seeing the smile of the astrologer as if nothing serious had
go to him. He asked the old man "You liar What makes you laugh at the time of end of your life ? Don't you fear death?
At this the old astrologer laughed loudly and said that the king would die only a week later he died. He added that he would wait in the air for a week only to receive the king's soul. All the stars and moon had declared it last night. So, that was the reason why he was laughing.
Hearing these loud words of the cunning palmist, the king turned pale. For some time he could not say anything. Then he ordered his men to drive away the wretched palmist because he could not tolerate this man. Ordering this, the king left the place quickly.

19.   Read the beginning of the following story and complete it in your own way. Give a title to it. Sheikh Saadi was a great poet in Iran. He used to lead a very simple life. Once on his way to the court of the king of Iran, he took shelter in a noble man's house for a night. He was then in very simple dressed.                           
Ans.                                     Sheikh Saadi's Wit / Tit For Tat
Sheikh Saadi was a great poet in Iran. He used to lead a very simple life. Once on his way to the court of the king of Iran, he took shelter in a noble man's house for a night. He was then in very simple night  The rich man did not treat him well. The treatment he received in the rich man's house offended him. But he did not say anything to him.
On the next day Sheikh Saadi went to the court of the king and was received with honour. He stayed' there for a few days. He composed some beautiful poems and entertained the king and his courtiers, He received rich gifts from the king. He put on a very rich and ornamented dress. Thus he was returning to his village. On his way he again visited that noble man's house.This time, the rich man was surprised to see SheikhSaadi in a rich ornamental dress. He was delighted to see the change in Sheikh Saadi and treated him with the best of the foods and comfort.
While eating, he was surprised to see the rich delicious food and understood the rich man's attitude. Then Sheikh Saadi instead of eating the food started putting them into the pockets of his rich cloth. .This surprised the rich man. "Why Are you putting the food into the pockets of your dress?" said the rich man. "I am doing so because these foods are meant for my dress, not for me", said Saadi. The rich man realized his earlier folly and became ashamed.

20.   Read the beginning of the following story and complete it in your own way. Give a title to it. A long time ago, the ,town of Hamelin in Germany was faced with a great problem. It became full of rats. The rats were so big and fierce that they fought against the dogs, killed the cats and bit the babies in the cradles........
Ans.                                      Consequence of Ingratitude / The Piper of Hamelin
A long time ago, the town of Hamelin in Germany was faced with a great problem. It became full of rats. The rats were so big and fierce that they fought against the dogs, killed the cats and bit the babies in the cradles. They ate up different types of foods and cut the papers and valuable documents into pieces. People had to face a big crisis. One day the city Mayor and the administrative officers called a meeting to solve the problem. They offered a prize for the person who would save them from the disturbing rats. At that time a pied piper agreed to take the proposal. Playing a melodious tune on his pipe the piper began to walk and all the rats followed him. At last the piper reached a hill near a sea and the rats one after another jumped into the sea. Thus the city was saved from the rats. Then the piper demanded his prize but the Mayor did not want to pay him fully. Saying nothing the piper went away. Now he played another tune on his pipe. The tune made all the nice babies of the city follow the piper and they never came back. Then the town of Hamelin became a city of cry and sigh because of Mayor's ingratitude.

21.    Read the beginning of the following story and complete it in your own way. Give a title to it.
Once a flippant scholar was going to a place by boat. It was the beginning of the summer season. The boatman set sail and the boat was advancing smoothly. ......................................
Ans.                                                         Intelligence of a Boatman
Once a flippant scholar was going to a place by boat. It was the beginning of the summer season. The boatman set sail and the boat was advancing smoothly. Everything was okay. At one moment of their conversation, the scholar asked the boatman, "Do you know history?" The boatman simply answered, "No". The scholar said that one third of his life was spoilt. He asked again whether he had read geography. The boatman replied his inability this time also. The scholar scolded him saying that one-eight of his life was spoilt. A few minutes later, the sky became covered with clouds. But suddenly, the river experienced stormy weather. It became- rough. Stormy wind began to blow around them. There were big waves on the river. The boatman indicated a possible danger in the river. He then asked the scholar whether he knew how to swim. But the scholar replied in the negative. The boatman then said, "the whole of your life is spoilt."




22. Read the beginning of the following story and complete it in your own way. Give a title to it.
One day a boy of twelve years was tending cattle by the side of a railway line. Suddenly he noticed that a small railway bridge was about to collapse..........................................
Ans.                                                  A young Hero
One day a boy of twelve years was tending cattle by the side of a railway line. Suddenly he noticed that a. small railway bridge was about to collapse. A little while later, he remembered that a mail train was supposed to cross the bridge in about half an hour. He realised that a terrible mishap was about to take place. He made up his mind to do something for saving many lives. A torn shirt which he wore was red. He put off it then and there, and began searching a stick or so. He found it within a few seconds between the two parallel rail lines. In the twinkling of an eye he tied the shirt at the one end of the stick and holding the other end he kept raising it high like a flag. The driver of the mail train from a safe distance came across the red like shirt under which he saw the boy as well. The driver sensed that something perilous in the line happened. So he began to brake gradually and the train stopped fully close to the very boy. In this way many lives were saved from being doomed for the heroic deed of the boy. Besides, govt was also able to avoid a vast loss.

23.   Read the beginning of the following story and complete it in your own way. Give a title to it.
There lived two boys in a village. One of them was rich and the other was poor. They were close friends. They thought they would keep their friendship until death. …........................
Ans.                                              Generousity of a Friend
There lived two boys in a village. One of them was rich and the other was poor. They were close friends. They thought they would keep their friendship until death. After passing SSC examination, the rich boy left the poor boy and went to the town to study in a famous college. He was studying in science to be an engineer. He was staying with his uncle.
The rich friend was passing his days well, but his poor friend was studying in a rural college. He was in dire state in his village home. He could not manage his academic expenses. So, he always remembered his rich friend, but could not communicate with him as he had no connection with him and did not know his whereabouts. The poor friend always remained worried for his rich friend. fie thought one day he must meet him and told everything to him.
By a curious twist of fate, the rich friend came to his village home and met his poor friend. The poor friend's joys knew no bounds. He thought that his rich friend would not recognise him. But surprisingly, the rich friend recognised him and gave him a surprise. He came to the village home and took his friend to the town to give him a job. The poor friend's sorrows ended.




24.   Read the beginning of the following story and complete it in your own way. Give a title to it.
One day some boys were playing cricket in a school ground. All on a sudden they heard a hue and cry nearby. The boys stopped playing then and there and went to the spot. They found some people lying on the road as an accident had just happened there……........................................
Ans.                                      Righteousness of Some School Boys
One day some boys were playing cricket in a school ground. All on a sudden they heard a hue and cry nearby. The boys stopped playing then and there and went to the spot. They found some people lying on the road as an accident had just happened there, They felt a great sympathy for them and took them to the nearest hospital. After a primary treatment most of the passengers left the hospital giving them thanks. Some of the passengers were severely injured. The boys informed their relatives. But the relatives were getting late to arrive there.. Without finding any alternative they took these severely injured passengers to a medical college hospital by an ambulance. The medical college was about 50 kilometres away. It took one and half an hour. When they arrived at the hospital, they found some of the relative waiting. Finally the injured passengers got the right treatment and recovered gradually. All of the boys felt relieved but happy. The passengers and their relatives gave heartfelt gratitude to them. This act of righteousness still works as their moral boost.

25.   Read the beginning of the following story and complete it in your own way. Give a title to it.
Once a school boy named Ahsanullah was going home after completing his class. When he was crossing the road, he saw an old woman lying on the road. She was senseless. There was nobody around her for help.  
Ans.                                                 A Kind-Hearted Boy
Once a school boy named Ahsanullah was going home after completing his class. When he was crossing the road, he saw an old woman lying on the road. She was senseless. There was nobody around her for help.
The road on which the woman lay senseless was a busy one. Speedy buses and trucks were plying on the road. And the woman could be run over by any vehicle. So the boy with the help of a passer-by brought the woman on the footpath. Then he called a rickshaw and took the woman to the nearby hospital. There the doctors at once examined the old woman. The doctors assured the boy that the condition of the woman was not serious and after first aid the old woman regained her sense.
Now Ahsanullah inquired of her whereabouts. The woman told him that she had come to this town from a nearby village. She came here to beg. But having heard a very loud sound of truck-horn, she was at a loss. And at one point, she lost her sense.
After hearing this, the boy took the woman to his house. He gave her good foods to eat. His mother also gave the woman some old clothes. On the boy's request his mother also gave the woman some money to go back to the village, And the woman prayed much for Ahsanullah and went back to her village happily.





26. Read the beginning of the following story and complete it in your own way. Give a title to it.
As I was walking home yesterday, a small man with a long pointed beard and only one arm stopped me and asked me the way to "Nur Manjil". I was very surprised because, that is my own house
Ans.                                             A Memorable Incident
As I was walking home yesterday, a small man with a long pointed beard and only one arm stopped me and asked me the way to  Nur Manjil I was very surprised because that is my own house. However, I thought that perhaps he had some business with other members of my family. So, without asking anything more, I helped the man to reach my house.
No sooner had I reached the main gate of our house with the stranger than my father saw the man. They first gazed at each other for some time. It seemed that they could not talk either for happiness or for sorrow. I marked another thing that tears were rolling down from their eyes. This surprising situation struck me dumb. Suddenly, they embraced each other and began weeping.
In the meantime, the other members of our family reached the place. Immediately after seeing the man, my grandmother addressed him as 'Nur Hossain!'. My another uncle also addressed him as "Bhaiya (brother)!"
Immediately, I remembered as I had heard from my father that the man is none but one of my uncles who joined the Liberation War. The name of our house has also been named after him. My joys knew no bounds at that moment. And now I feel proud that one of my uncles fought for the sake of our country. I remember this unexpected but remarkable incident even now.

27.      Read the beginning of the following story and complete it in your own way. Give a title to it. Once on a winter day a farmer was coming back home. On his way home he saw a kitten lying by the side of a road. The kitten was very weak and was about to die from cold.
The farmer..........................................
Ans.                                                A Kind Farmer and an Ungrateful Kitten
Once on a winter day a farmer was coming back home. On his way home he saw a kitten lying by the side of a road. The kitten was very weak and was about to die from cold.
The farmer was a kind-hearted man. He felt pity for the kitten. He took the kitten home and wrap it with warm cloth. After a while the kitten became all right and started to play with his daughter.
The farmer's little daughter wanted to take the kitten on her lap. But as soon as the little girl caught it, the ungrateful kitten scratched and bit on the girl's hand. Blood came out from the wound. Neighbours, and his family members became surprised to see this. They advised the farmer to keep the kitten out of the home.
The farmer also became very angry. He took the kitten away and threw it in a distant field.




28.      Read the following outlines and develop them into a complete story. Give a suitable title to your story.
A woodcutter had a donkey — went to the forest — loaded wood on its back — sat upon the logs — a man asked the reason — the wood-cutter wanted to save his weak donkey — the man laughed at him.
Ans.                                                   A Foolish Woodcutter and His Donkey
There was a woodcutter. He had a donkey. One day he went to the forest with his donkey to bring firewood. In the forest he cut some logs and put them on the back of the donkey. The woodcutter did not sit on the saddle. He sat on the logs. Then he started his return journey.
After a few minutes the woodcutter met a man on the way. He saw him and laughed. He asked him, "Why did you not sit on the saddle?"
The woodcutter looked at the man and said, "You're a foolish man. You don't realise my donkey is weak. It can't bear my weight. So I have sat on the logs. Now my weight is on the logs and not on the donkey. If I sit on the saddle, my weight will be on the donkey. Then it'll not be possible for the donkey to bear the weight. Poor animal! I must help him:"
The man thought that the woodcutter was foolish. So, he laughed and went away.

29.     Read the beginning of the following story and complete it in your own way. Give a title to it.
There was a little boy. His name was Babul. He was very intelligent. He used to play with his playmates after completing his lessons.
Once he was playing hide and seek................................................................
Ans.                                                    An Intelligent Boy
There was a little boy. His name was Babul. He was very intelligent. He used to play with his playmates after completing his lessons.
Once he was playing hide and seek with his friends. They were playing by a washer man's house. The washer man put water in big pots. Suddenly -one of his playmates fell into a pot while he was crossing a well beside the pot. All of his playmates came to the pot, but they could not pull him out of the pot. Because the pot was too big to move. All friends were trying best to find out new way to save him. But all attempts are proved in vain.
Babul then made a.plan. He fetched a stone and hit the pot with all his strength. As a result a big hole was made and water came out through the hole. Thus the boy was saved. He was then immediately sent to the local hospital. The villagers went there and heard everything. They praised Babul's ready wit and bravery.




30.     Read the following outlines and develop them into a complete story. Give a suitable title to your story.
In the month of Chaitra a thirsty crow — found a jar — but failed to drink from the bottom of the jar — put some small pieces of stone inside it — the water came up — finally he reached the water.
Ans.                                                    A Thirsty Crow
It was the month of Chaitra. The day was very hot. There was scorching sun all around. There was no water anywhere. Farmers were taking rest under trees.
In the meantime a crow became very thirsty. He flew here and there for water, but he found no water. At last he found a jar. He became glad and went to the jar to drink water. But the water was at the bottom of the jar. He tried his best to drink water but his bill could not reach it. It became very sad and hopeless.
Suddenly, the crow saw some small pieces of stone near the jar. A good idea came into his head. He picked up the stones and dropped them into the jar one by one. As a result, the water came up to the brim. The crow then drank water from it to his heart's content and flew away with a joyful mind.

31.     Read the following outlines and develop them into a complete story. Give a suitable title to your story.
A woodcutter living in a village — cutting wood near a pond — suddenly lost his axe into water —sitting there sadly — a beautiful fairy appeared — showed him gold and silver axes — the woodcutter refused — the fairy showed him the lost axe — the fairy became pleased.
Ans.                            An Honest Woodcutter and the Beautiful Fairy
There lived a woodcutter in a village. One day he was cutting wood near a pond. Suddenly, his axe fell into the pond. The pond was very deep. The woodcutter did not know how to swim or dive. So, he was sitting there sadly.
Then a wonderful thing happened. A beautiful fairy appeared before the woodcutter. She asked him in a sweet voice, "Why are you so sad? Why are you not cutting wood?"
The woodcutter replied sorrowfully, "My axe has fallen into the pond. I can't cut wood now."
The fairy then showed him an axe made of silver. She asked him if it was his axe. The woodcutter saw the axe and said, "It's not my axe."
The fairy showed him another axe made of gold. She asked, "Is this the axe that you lost?" The woodcutter said, "No, it's not. My axe is made of iron. It was old." The fairy showed him the lost axe with a wooden handle. The woodcutter then said happily that it was his axe.
The fairy became very pleased and gave him the gold and the silver axes too. Then the woodcutter became rich and began to live happily. So, we must keep in mind that honesty is always rewarded.




32.     Read the beginning of the following story and complete it in your own way. Give a title to it. Last Friday I went to bed at 11 pm. Suddenly, I woke up hearing hue and cry at a little distance. I got up from bed and rushed to the spot.
I found that a cottage was burning..............
Ans.                                                   A Fire Accident
Last Friday I went to bed -at I I pm. Suddenly, I woke up hearing a hue and cry at a little distance. I got up from bed and rushed to the spot.
I found that a cottage was burning and the fire was rising up to the sky. The fire had already engulfed one-third of the cottage and was spreading rapidly to damage the whole. But like me the neighbours were rushing to the cottage and trying to extinguish the fire. Seeing the fire, most of them came with jugs, buckets, pots, pitchers and so on.
It was fortunate that there was a pond nearby. The women were fetching water with the pots and the men were throwing it at the fire. Within fifteen minutes a united effort brought the fire under control and people succeeded in extinguishing it. The dwellers of the cottage were a couple with their only son. The man of the cottage used to earn his livelihood by working in the field. The fire incident made them seriously upset. This accident left nothing for them. But the neighbours assured them of financial assistance. I was greatly shocked seeing the burnt cottage. But the fellow feelings made it easy for me to forget the incident. So, united effort is. needed to combat any type of accident.

33.     Read the following outlines and develop them into a complete story. Give a suitable title to your story.
A crow wanted to be white like a goose — noticed the goose always in water — he got into water — but remained black as before — then remained in water for a long time — long starvation — but could not turn white — ultimately died.
Ans.                                   An Ambitious Crow and His Sad End
A crow said to a goose, "Really, how beautiful you are! What a fair complexion you have! If I were as white as you are! How bad it is for me to be black!"
The crow noticed that the goose always stays in the water. He. thought that he would be as white as the goose if he stays in water. So, he got into water. He then got out of water but found him as black as he was before.
Next, he thought that he would be on the water for a long time as the goose does. Thinking so he started staying in the water day after day.
He used to collect food flying here and there when he was in the land. Now he was starving as he did not go out looking for food but could not turn white. Eventually, he died due to his starvation for a long time. So, before following others, ultimate results should be justified well.

